
call: clean all
all: c
	make -C ed25519

load-html: clean all
	find html | grep -v '^html$$' | xargs ros $@.lisp

c:
	mkdir -p c/f
	mkdir -p c/0/genesis/lsp
	mkdir -p c/0/genesis/nxt
	mkdir -p c/0/genesis/blk
	mkdir -p c/0/genesis/xtn
	mkdir -p c/0/genesis/tmp
	cp verify-*.lisp c/0/genesis/lsp
	cp verify-*.lisp c/0/genesis/nxt
clean:
	make -C ed25519 clean
	rm -fr *~ ?
	ls -la
	tree .
