(load "ed25519.lisp")

(defun process-file (fn)
  (let*((sha (ed25519::sha512-file fn))
	(cmd (format nil "cp ~a c/f/~a" fn sha)))
    (format t "~a~%" cmd)
    (uiop:run-program cmd)))

(defun main (&rest argv) (mapcar #'process-file argv))
